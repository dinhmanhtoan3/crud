﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Models
{
    public partial class ProductColor
    {
        public long Id { get; set; }
        public long? ProductId { get; set; }
        public long? ColorId { get; set; }

        public virtual Color Color { get; set; }
        public virtual Product Product { get; set; }
    }
}
