﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Models
{
    public partial class User
    {
        public User()
        {
            ProductCreateByNavigations = new HashSet<Product>();
            ProductEditByNavigations = new HashSet<Product>();
        }

        public long Id { get; set; }
        public string UserName { get; set; }
        public string Passwords { get; set; }
        public string Fullname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int? UserTypes { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsActive { get; set; }
        public int? DisPlayOrder { get; set; }

        public virtual ICollection<Product> ProductCreateByNavigations { get; set; }
        public virtual ICollection<Product> ProductEditByNavigations { get; set; }
    }
}
