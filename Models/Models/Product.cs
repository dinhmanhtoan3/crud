﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Models
{
    public partial class Product
    {
        public Product()
        {
            ProductColors = new HashSet<ProductColor>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public string ListImage { get; set; }
        public long? CategoryId { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsActive { get; set; }
        public int? DisPlayOrder { get; set; }
        public decimal? Price { get; set; }
        public decimal? Sales { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreateBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditBy { get; set; }
        public long? BrandId { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual Category Category { get; set; }
        public virtual User CreateByNavigation { get; set; }
        public virtual User EditByNavigation { get; set; }
        public virtual ICollection<ProductColor> ProductColors { get; set; }
    }
}
