﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Models
{
    public partial class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public long? ParentId { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsActive { get; set; }
        public int? DisPlayOrder { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
