﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Models
{
    public partial class Color
    {
        public Color()
        {
            ProductColors = new HashSet<ProductColor>();
        }

        public long Id { get; set; }
        public string Code { get; set; }
        public string DisPlayName { get; set; }
        public int? DisPlayOrder { get; set; }
        public string Slug { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsActive { get; set; }

        public virtual ICollection<ProductColor> ProductColors { get; set; }
    }
}
