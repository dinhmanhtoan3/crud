﻿using System.Linq;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Services
{
    public class CategotyServices
    {
        private readonly aranozContext _context;
        public CategotyServices(aranozContext context)
        {
            _context = context;
        }

        public void addCategory(Category entity)
        {
            _context.Categories.AddAsync(entity);
            _context.SaveChangesAsync();
        }

        public void updateCategory(Category entity)
        {
            _context.Categories.Update(entity);
            _context.SaveChangesAsync();
        }
        public void deleteCategory(Category entity)
        {
            _context.Categories.Remove(entity);
            _context.SaveChangesAsync();
        }
        public Category findById(long Id)
        {
           return _context.Categories.FirstOrDefault(x=>x.Id == Id);
        }
        public List<Category> ListCategory(long Id)
        {
            return _context.Categories.Where(x=>x.IsDelete == false && x.IsActive == true).ToList();
        }


    }
}
